<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8" />
<style type="text/css">
#up {	
	height: 5em;
	width: 100%;
	background-color: transparent;
	font-size: 80%;
	font-weight: bold;
	color: #ffffff;
}
#down {	
	height: 5em;
	width: 100%;
	background-color: transparent;
	font-size: 80%;
	font-weight: bold;
	color: #ffffff;
}
#left {	
	
	height: 288px;
	width: 145px;
	background-color: transparent;
	font-weight: bold;
	color: #ffffff;
}
#right {	
	
	height: 288px;
	width: 145px;
	background-color: transparent;
	font-weight: bold;
	color: #ffffff;
}
div.centre
{ 
	background-image:url('ogp1gui.jpg');
	display: block;
	margin-left: auto;
	margin-right: auto;
	max-height: 600px; 
	max-width: 850px;
	min-width: 850px;
	min-height: 600px;
}

input.padbutton
{ 
width:80px;
 color:white;
 height:100px;
}

#pad{
	color: white;
	font-weight: bold;
}

#bottom{
	max-height: 300px;
	columns: 300px 2;
	-webkit-columns: 300px 2;
}
body {	
	
	text-align: center;
	background-color: #000000;
     }

</style>

<script src="http://192.168.42.1/jquery-1.11.0.min.js"></script>
<script>
      $(function(){
       var ws;
       var logger = function(msg){
         var now = new Date();
         var sec = now.getSeconds();
         var min = now.getMinutes();
         var hr = now.getHours();

         $("#log").html($("#log").html() + "<br/>" + hr + ":" + min + ":" + sec + " " + msg);
	 $("#log").scrollTop($("#log")[0].scrollHeight);

	 packet = msg.toString();
	 var res = packet.split("_", 4);

	 var h = res.slice(0);

	 var x = res.slice(1);
	 var y = res.slice(2);
	 var p = res.slice(3);

	 var h = h.toString();
	 var p = p.toString();
	 var xpos = parseInt(x);
	 var ypos = parseInt(y);

	 var c = document.getElementById("myCanvas");
	 var ctx = c.getContext("2d");
	 xpos = xpos * 20;
	 ypos = -ypos * 20 ;
	 var x2 = xpos + 20;
	 var y2 = ypos + 20;
	 var y3 = ypos + 35;
	 var d = "d";

	 var thp1 = "/images/thumbs/thumb";
	 var thp2 = ".png";
	 var thumbpath = thp1.concat(p,thp2);

         $("#log").html($("#log").html() + "<br/>" + ypos);
	 $("#log").scrollTop($("#log")[0].scrollHeight);

	 if (h = d){
	  var imageObj = new Image();
	  imageObj.onload = function(){
	
	    ctx.drawImage(imageObj, xpos, ypos);
	   };
	  imageObj.src = thumbpath;

/*
	  ctx.fillStyle = "FFFFFF";
	  ctx.fillRect(xpos, ypos, x2, y2);
	  ctx.font="10px Arial";
          ctx.fillText(p, x2, y3);
*/
	 }


	}

	var up = function() {
	  var upper = 'g';
	  if (upper.length > 0)
	    ws.send(upper);
	   $("#upper").val(msg);
	}

	var sender = function() {
	  var msg = $("#msg").val();
	  if (msg.length > 0)
	    ws.send(msg);
	   $("#msg").val(msg);
	}

	ws = new WebSocket("ws://192.168.42.1:8888/ws");
	ws.onmessage = function(evt) {

	  logger(evt.data);


	};
	ws.onclose = function(evt) {
	    $("#log").text("Connection Closed");
	    $("#thebutton #msg").prop('disabled', true);
	};
	ws.onopen = function(evt) { $("#log").text("OGP-- SOCKET OPEN"); };

	$("#msg").keypress(function(event) {
	    if (event.which == 13) {
	      sender();
	    }
	});

	$("#up").click(function(){
	    ws.send('y');
	});

	$("#down").click(function(){
	    ws.send('g');
	});

	$("#left").click(function(){
	    ws.send('h');
	});

	$("#right").click(function(){
	    ws.send('j');
	});

	$("#in").click(function(){
	    ws.send('f');
	});

	$("#out").click(function(){
	    ws.send('t');
	});

	$("#short").click(function(){
	    ws.send('q');
	});

	$("#long").click(function(){
	    ws.send('a');
	});

	$("#map").click(function(){
	    ws.send('n');
	});

	$("thebutton").click(function(){
	    sender();
	});
     });
   </script>
</head>

<body>
 <div class="centre" id="main">
  <div id="top">
  <input type="image" id="up" value="UP" /><br>
  </div>

  <div id="middle">
  <input type="image" id="left" value="LEFT"  ;/>
  <iframe src="http://192.168.42.1:8080" width="544" height="288" frameborder="0" scrolling="no">...</iframe>
  <input type="image" id="right" value="RIGHT"  />
  </div>

  <div>
  <input type="image" id="down" value="DOWN" />
  </div>

  <div id="bottom">
   <div id="pad">
   <input class= "padbutton" type="image" id="in"  value="IN" />
   <input class= "padbutton" type="image" id="out" value="OUT" />
   <input class= "padbutton" type="image" id="short" value="SHORT STEP" />
   <input class= "padbutton" type="image" id="long" value="LONG STEP" />
   <input class= "padbutton" type="image" id="map" value="MAP" /><br><br><br>
   </div>
   <div id="E" >
    <div id="log" style="overflow:scroll; width:200px; height:100px; color:white; background-color:#000000; margin: 1px; text-align:left">Messages go Here</div>
   <div id="sender" style="text-align:left">
   <input type="text" id="msg" style="background:#ffffff; width:100px " />
   <input type="button" id="thebutton" value="Send" />
   </div>
   </div>

  </div>
<div><br><br>
<canvas id="myCanvas" width="544" height="288" style="border:1px solid#ffffff;"></canvas><canvas id="myCanvas" width="544" height="288" style="border:1px solid#000000;"></canvas>
</div>
 </div>
</body>
</html>
