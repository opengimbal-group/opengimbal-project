## OGP Library of Functions
## currently holding only MAP MODE 
## which is uninterruptable 


#!/usr/bin/env python2     
# ogp.py

import time
import serial
from SimpleCV import Camera, Image


mySet = set()    ## holds the map array data
spiral = True     ## JUNK - attempting to write an "off" switch

brightpixels = 0       ## for use inside histogram
darkpixels = 0

l = int(4)            ## default map size (in photos)
s = serial.Serial('/dev/ttyUSB0', 9600)    ## probably another unnecessary line



class so(object):        ## SEEK OUT  and  MAP

    def __init__(self, side, c, m, js, wsh, wsh2):    ## i was working fast and some of this is unnecessary and a mess
        self.js = js     ## js is the main image socket jpeg framebuffer 
        self.wsh = wsh         ## wsh is the prefix of a log websocket command stored here unnecessarily 
        self.wsh2 = wsh2     ## wsh2 is the actual instance info of the log websocket so i can write to it
        self.m = m           ## im pretty sure m does nothing now
        self.c = c            ## c is the camera info
        self.l = side       ## l is the width / height of the map to be drawn (in photos)
        self.brightpixels = 11
        self.darkpixels = 11
        self.x = 0
        self.y = 0
        self.w = 0       ## w ends up being the number of bright pixels
        self.p = 0         ## p is photo index number


    def histo(self):         ##   this def determines if the camera has registered any light
        i = 0               ## if yes then the photo is processed into the system
        js = self.js
        wsh = self.wsh
        m = self.m
        w = self.w
        c = self.c
        brightpixels = 0
        darkpixels = 0
        wsh2 = self.wsh2
        s.write('q')
        img1 = c.getImage()
        ##img1.save(d)
        hist = c.getImage().histogram(20)


        while i < 20:
            if (i < 10):
                darkpixels = darkpixels + hist[i]
                self.darkpixels = darkpixels
            else:
                brightpixels = brightpixels + hist[i]
                self.brightpixels = brightpixels
            i = i + 1

        if (darkpixels > 0):          ## looks like my variables are named backwards here
            print "bright"            ##  this if-block is for processing the image
            x = self.x
            y = self.y
            w = darkpixels
            p = self.p
            p = p + 1
            self.w = darkpixels

            
            img1 = c.getImage()

            
            thumbnail = img1.crop(100,0,300,300)    ## make a thumbnail
            thumbnail = thumbnail.dilate(10)
            thumbnail = thumbnail.scale(20,20)

            thumb1 = "images/thumbs/thumb"
            thumb3 = ".png"
            thumbpath = thumb1 + str(p) + thumb3
            thumbnail.save(thumbpath)             ## store it 

       
            blobs = img1.findBlobs()                         ## find main blob of similar color
            img1.drawCircle((blobs[-1].x,blobs[-1].y),30,color=(255,255,255))
            img1.drawCircle((blobs[-1].centroid()),10,color=(255,100,100))
            print blobs[-1].meanColor()

            
            img1.drawText("ogp: mapping", 10, 10, fontsize=50)          ##   mark data onto image
            img1.drawText(str(x), 10, 200, color=(255,255,255), fontsize=50)
            img1.drawText(str(y), 50, 200, color=(255,255,255), fontsize=50)
            img1.drawText(str(w), 10, 250, color=(255,255,255), fontsize=50)
            img1.save(js.framebuffer)                                 ## send it to the browser

            
            pth1 = "images/image"        
            pth3 = ".png"
            pth = pth1 + str(p) + pth3
            print pth
            img1.save(pth)        ## save original image

            
            self.p = p               ##   add data to your map array
            mySet.add((p,x,y,w))
            xm = x*10+100
            ym = y*10+100
            xm2 = xm + 4
            ym2 = ym + 4
            self.mySet = mySet
            
            wshx = str(self.x)       ## pass data over socket
            wshy = str(self.y)
            wsh.write_message(wsh2, "d_" + wshx + "_" + wshy + "_" + str(p) )
        
        
        
        else:           ## if no light then skip ahead
            wshx = str(self.x)
            wshy = str(self.y)
            wsh.write_message(wsh2, "current position  " + wshx + "," + wshy)

            print "dark"


    def run(self):      ##    this def defines the motion of the mapping as back and forth 
        l = self.l           ##         starting at top-left 
        x = 0
        y = 0
        l = self.l
        time.sleep(1)       ## intermitten sleeping is buffering this process
        s.write('s')          ## i frequently state my step length because of some strange confusion
        time.sleep(1)         
        print "PPMS : 200 approx"

        print "Starting: TOP-LEFT , Scan of: ", self.l
        countdownA = int(self.l)
        countdownB = int(self.l)

        while countdownA > 0:

            while countdownB >0:
                print "right"
                countdownB-=1
                s.write('s')
                s.write('h')      ## H = right 
                x = x + 1      ## mark where you moved to
                self.x = x       
                time.sleep(1)
                elf = self.histo()    ## the elf takes the picture and processes it. thanks, elf
                time.sleep(1)         
                
            countdownB = int(self.l)

            print "down"
            s.write('s')
            s.write('y')     ## y = down
            y = y - 1
            self.y = y
            time.sleep(1)
            elf = self.histo()
            time.sleep(1)
            countdownA -=1

            while countdownB >0:
                print "left"
                countdownB -=1
                s.write('s')
                s.write('j')
                x = x - 1
                self.x = x
                time.sleep(1)
                elf = self.histo()
                s.write('q')
                time.sleep(1)

            countdownB = int(self.l)

            print "down"
            s.write('s')
            s.write('y')
            y = y - 1
            self.y = y
            time.sleep(1)
            elf = self.histo()
            time.sleep(1)
            countdownA -=1

        else:
            wsh = self.wsh
            wsh2 = self.wsh2

            wsh.write_message(wsh2, str(mySet) )    ## send completed map-data array via socket

            print "done"



if __name__ == '__main__'  :        ## since this file is now only to be accessed as a module
    foo = so(2)                     ## this code never happens
    foo.histo()
    foo.run()
else:
   pass




