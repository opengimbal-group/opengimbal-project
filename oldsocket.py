import tornado.httpserver
import tornado.websocket
import tornado.ioloop
import tornado.web
import time
import serial
from SimpleCV import *

ser = serial.Serial('/dev/ttyUSB0', 9600)


c = SimpleCV.Camera(0,{ "width": 160, "height": 120 })
c2 = SimpleCV.Camera(1,{ "width": 544, "height": 288 })
js = SimpleCV.JpegStreamer('0.0.0.0:8080')
time.sleep(4)
c2.getImage().save(js.framebuffer)

class WSHandler(tornado.websocket.WebSocketHandler):
    def open(self):
        print 'New connection was opened'
        self.write_message("telescope listening")

    def on_message(self, message):
        print 'Incoming message:', message
        self.write_message("echo: " + message)
        if message =='j':
            print "j"
            ser.write('j')
            
        if message =='h':
            print "h"
            ser.write('h')
            
        if message =='n':
            print "n"
            ser.write('n')
            
        if message =='y':
            print "y"
            ser.write('y')
            
        if message =='g':
            print "g"
            ser.write('g')
            
        if message =='t':
            print "t"
            ser.write('t')
            
        if message =='f':
            print "f"
            ser.write('f')
            
        if message =='q':
            print "q"
            ser.write('q')
            
        if message =='a':
            print "a"
            ser.write('a')
            
        time.sleep (1)
        c2.getImage().save(js.framebuffer)

    def on_close(self):
        print 'Connection was closed...'

application = tornado.web.Application([
    (r'/ws', WSHandler),
])


if __name__ == "__main__":
    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(8888)
    tornado.ioloop.IOLoop.instance().start()
        
