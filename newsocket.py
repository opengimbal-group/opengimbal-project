##!/usr/bin/env python2
## newsocket.py


import tornado.httpserver
import tornado.websocket
import tornado.ioloop
import tornado.web
import time
import serial
from SimpleCV import *
from ogp import *

s = serial.Serial('/dev/ttyUSB0', 9600)


c = SimpleCV.Camera(0,{ "width": 544, "height": 288 })
##c2 = SimpleCV.Camera(1,{ "width": 544, "height": 288 })
js = SimpleCV.JpegStreamer('0.0.0.0:8080')
time.sleep(4)
c.getImage().save(js.framebuffer)


class WSHandler(tornado.websocket.WebSocketHandler):


    def open(self):
        print 'New connection was opened'
        self.write_message("telescope listening")
        self.x = 0
        self.y = 0
        self.w = 0
        self.p = 0


    def on_message(self, message):

        print 'Incoming message:', message
        x = self.x
        y = self.y
        if message =='j':
            print "j"
            s.write('j')
            x = x + 1
            self.x = x
            self.write_message("echo: " + message + " right " )

        if message =='h':
            print "h"
            s.write('h')
            x = x - 1
            self.x = x
            self.write_message("echo: " + message + " left" )

        if message =='n':
            print "n"

            self.write_message("echo: " + message + " map" )
            m = int(5)
            map = so('4', c, m, js)
            map.histo()
            map.run()
            print map.mySet

        if message =='y':
            print "y"
            s.write('y')
            y = y + 1
            self.y = y
            self.write_message("echo: " + message + " up   ")

        if message =='g':
            print "g"
            s.write('g')
            y = y - 1
            self.y = y
            self.write_message("echo: " + message + " down   ")

        if message =='t':
            print "t"
            s.write('t')
            self.write_message("echo: " + message + " focus out")            
        if message =='f':
            print "f"
            s.write('f')
            self.write_message("echo: " + message + " focus in")            
        if message =='q':
            print "q"
            s.write('q')
            self.write_message("echo: " + message + " toggle to short step")
        if message =='a':
            print "a"
            s.write('a')
            self.write_message("echo: " + message + " toggle to long step")
        self.write_message("to: " + str(x) + " , " + str(y))
        x = self.x
        y = self.y
        w = self.w
        img1 = c.getImage()
        img1.drawText("ogp", 10, 10, fontsize=50)
        img1.drawText(str(x), 10, 200, color=(255,255,255), fontsize=50)
        img1.drawText(str(y), 50, 200, color=(255,255,255), fontsize=50)
        img1.drawText(str(w), 10, 250, color=(255,255,255), fontsize=50)
        img1.save(js.framebuffer)

    def on_close(self):
        print 'Connection was closed...'

application = tornado.web.Application([
    (r'/ws', WSHandler),
])


if __name__ == "__main__":
    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(8888)
    tornado.ioloop.IOLoop.instance().start()
        
